//
//  Film.swift
//  ITSTestIMDB
//
//  Created by Ivan Babkin on 09.08.2018.
//  Copyright © 2018 Ivan Babkin. All rights reserved.
//

import UIKit

struct SearchFilmInfo {
    var id: String
    var title: String
}

class Film: NSObject {
    var id: String
    var title: String
    var year: String
    var imdbRating: String?
    var country: String?
    var director: String?
    var actors: String?
    var plot: String?
    var poster: UIImage?
    var posterPath: String?
    
    init(id: String, title: String, year: String, imdbRating: String?, country: String?, director: String?, actors: String?, plot: String?, posterPath: String?) {
        self.id = id
        self.title = title
        self.year = year
        self.imdbRating = imdbRating
        self.country = country
        self.director = director
        self.actors = actors
        self.plot = plot
        self.posterPath = posterPath
    }
    
}
