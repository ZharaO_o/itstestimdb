//
//  FilmDetailViewController.swift
//  ITSTestIMDB
//
//  Created by Ivan Babkin on 09.08.2018.
//  Copyright © 2018 Ivan Babkin. All rights reserved.
//

import UIKit

class FilmDetailViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var diractorLabel: UILabel!
    @IBOutlet weak var actorsTextView: UITextView!
    @IBOutlet weak var plotTextView: UITextView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    var imdbNetwork = IMDBNetwork()
    var film: Film?
    var filmId: String?
    
    // MARK: - lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let id = filmId {
            loadFilm(by: id)
        }
        else {
            showError(message: "Unknown error")
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.contentSize = contentView.frame.size
    }
    
    
    // MARK: - Data display
    
    func setupContent() {
        titleLabel.text = film?.title ?? "--"
        ratingLabel.text = "\(film?.imdbRating ?? "--") / 10"
        countryLabel.text = film?.country ?? "--"
        diractorLabel.text = film?.director ?? "--"
        actorsTextView.text = film?.actors ?? "--"
        plotTextView.text = film?.plot ?? "--"
        
        loadPoster()
        view.layoutSubviews()
    }
    
    
    // MARK: - Data loading
    
    func loadPoster() {
        if let path = film?.posterPath {
            imdbNetwork.loadPoster(url: URL(string: path)!) { (image, error) in
                if let image = image {
                    DispatchQueue.main.async {
                        self.posterImageView.image = image
                    }
                }
                else {
//                    self.posterImageView.image = UIImage(named: "defaultPoster")
                }
            }
        }
    }
    
    func loadFilm(by id: String) {
        loadingIndicator.startAnimating()
        contentView.isHidden = true
        
        imdbNetwork.loadFilm(by: id) { (film, error) in
            DispatchQueue.main.async {
                self.loadingIndicator.stopAnimating()
                self.loadingIndicator.removeFromSuperview()
                self.contentView.isHidden = false
            }
            
            if let film = film {
                self.film = film
                
                DispatchQueue.main.async {
                    self.setupContent()
                }
            }
            else {
                let message = error?.localizedDescription ?? "Unknown error"
                
                DispatchQueue.main.async {
                    self.showError(message: message)
                }
            }
        }
    }

    func showError(message: String) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
}
