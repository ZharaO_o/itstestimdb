//
//  SearchFilmsTableViewController.swift
//  ITSTestIMDB
//
//  Created by Ivan Babkin on 09.08.2018.
//  Copyright © 2018 Ivan Babkin. All rights reserved.
//

import UIKit

fileprivate extension UInt {
    func roundUpDivision(num: UInt) -> UInt {
        let mod = self % num
        let division = self / num
        
        return mod == 0 ? division : division + 1
    }
}


class SearchFilmsTableViewController: UITableViewController, UISearchBarDelegate {
    
    // MARK: - Constants
    
    private let FilmDetailSegueInentifier = "FilmDetailSegue"
    
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    
    // MARK: - Properties
    
    var searchResults = [SearchFilmInfo]()
    var page: UInt = 0
    var totalPages: UInt = 0
    var imdbNetwork = IMDBNetwork()
    
    var noDataLabel: UILabel?
    
    // MARK: - lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        noDataLabel = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: tableView.bounds.width, height: tableView.bounds.height))
        noDataLabel!.text = "Введите название фильма"
        noDataLabel!.textAlignment = .center
        
        let bottomIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        bottomIndicator.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
        
        tableView.tableFooterView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        tableView.tableFooterView?.isHidden = true
    }
    

    // MARK: - Table view data source & delegate

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchResults.count > 0 {
            tableView.separatorStyle = .singleLine
            tableView.backgroundView = nil
        }
        else {
            tableView.separatorStyle = .none
            tableView.backgroundView = noDataLabel
        }
        
        return searchResults.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = searchResults[indexPath.row].title

        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
            loadNextPage()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: FilmDetailSegueInentifier, sender: searchResults[indexPath.row])
    }
    
    
    //MARK: - Table view scroll delegate
    
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        tableView.endEditing(true)
    }
    
    
    // MARK: - UISearchBarDelegate

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        tableView.endEditing(true)
        
        if let searchText = searchBar.text, searchText.count > 0 {
            searchResults.removeAll()
            tableView.reloadData()
            
            page = 0
            totalPages = 0
            
            loadNextPage()
        }
    }
    
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == FilmDetailSegueInentifier {
            let searchFilm = sender as! SearchFilmInfo
            let dvc = segue.destination as! FilmDetailViewController
            dvc.filmId = searchFilm.id
        }
    }
    
    
    // MARK: - Page loading
    
    func loadNextPage() {
        if totalPages == 0 || page != totalPages {
            if let searchText = searchBar.text, searchText.count > 0 {
                tableView.tableFooterView?.isHidden = false
                let indicator = tableView.tableFooterView as? UIActivityIndicatorView
                indicator!.startAnimating()
                
                imdbNetwork.loadFilms(by: searchText, page: page + 1) { (searchResults, totalResults, error) in
                    DispatchQueue.main.async {
                        indicator!.stopAnimating()
                        self.tableView.tableFooterView?.isHidden = true
                    }
                    
                    if let searchResults = searchResults, let totalResults = totalResults {
                        self.totalPages = self.totalPages == 0 ? UInt(totalResults).roundUpDivision(num: UInt(searchResults.count)) : self.totalPages
                        self.page += 1
                        
                        DispatchQueue.main.async {
                            self.appendDataToTableView(searchResults)
                        }
                    }
                    else {
                        let message = error?.description ?? "Unknown error"
                        
                        DispatchQueue.main.async {
                            self.showError(message: message)
                        }
                    }
                }
            }
        }
    }
    
    func appendDataToTableView( _ searchResults: [SearchFilmInfo]) {
        if self.page == 1 {
            self.searchResults = searchResults
            self.tableView.reloadData()
        }
        else {
            self.searchResults.append(contentsOf: searchResults)
            var indexPaths = [IndexPath]()
            
            for i in self.searchResults.count - searchResults.count ... self.searchResults.count - 1 {
                indexPaths.append(IndexPath(row: i, section: 0))
            }
            
            self.tableView.insertRows(at: indexPaths, with: .none)
        }
    }
    
    func showError(message: String) {
        if self.searchResults.count == 0 {
            self.tableView.backgroundView = self.noDataLabel
        }
        
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
}
