//
//  IMDBService.swift
//  ITSTestIMDB
//
//  Created by Ivan Babkin on 09.08.2018.
//  Copyright © 2018 Ivan Babkin. All rights reserved.
//

import UIKit
import Foundation

struct SearchFilmResponce {
    var films: [SearchFilmInfo]?
    var totalResults: Int?
}

class IMDBNetwork: NSObject {
    
    typealias JSONDictionary = [String: Any]
   
    
    // MARK: - Constants
    
    private let Host = "http://www.omdbapi.com/?apikey=a79df0a3&"
    private let DataType = "movie"
    private let PlotType = "full"
    
    
    // MARK: - Public properties
    
    var session = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?
    
    
    // MARK: - Main load method
    
    private func load(url: URL, completion: @escaping (Data?, Error?) -> ()) {
        dataTask?.cancel()
        
        dataTask = session.dataTask(with: url) { (data, urlResponse, error) in
            var responseError: Error?
            var responseData: Data?
            
            if let error = error {
                responseError = error
            }
            else {
                if let data = data, let urlResponse = urlResponse as? HTTPURLResponse, urlResponse.statusCode == 200 {
                    responseData = data
                }
                else if let urlResponse = urlResponse as? HTTPURLResponse, urlResponse.statusCode != 200 {
                    responseError = IMDBNetworkError.HTTPResponseError(urlResponse.statusCode)
                }
            }
            
            completion(responseData, responseError)
        }
        
        dataTask?.resume()
    }
    
    
    //MARK: - Public Methods For Loading
    
    func loadFilms(by partTitle: String, page: UInt, completion: @escaping ([SearchFilmInfo]?, Int?, Error?) -> ()) {
        let urlString = "\(Host)type=\(DataType)&s=\(partTitle)&page=\(page)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)
        let url = URL(string: urlString!)
        
        load(url: url!) { (data, error) in
            var responseError: Error?
            var searchResponse: SearchFilmResponce?
            
            if let data = data {
                do {
                    searchResponse = try self.searchDataProcessing(data)
                }
                catch {
                    responseError = error
                }
            }
            else {
                responseError = error
            }
            
            completion(searchResponse?.films, searchResponse?.totalResults, responseError)
        }
    }
    
    func loadFilm(by id: String, completion: @escaping (Film?, Error?) -> ()) {
        let urlString = "\(Host)&plot=\(PlotType)&i=\(id)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)
        let url = URL(string: urlString!)
        
        load(url: url!) { (data, error) in
            var responseError: Error?
            var film: Film?
            
            if let data = data {
                do {
                    film = try self.filmDataProcessing(data)
                }
                catch {
                    responseError = error
                }
            }
            else {
                responseError = error
            }
            
            completion(film, responseError)
        }
    }
    
    func loadPoster(url: URL, completion: @escaping (UIImage?, Error?) -> ()) {
        load(url: url) { (data, error) in
            if let data = data {
                completion(UIImage(data: data), nil)
            }
            else {
                completion(nil, error)
            }
        }
    }
    
    
    //MARK: - DataProcessing
    
    private func dataProcessing(_ data: Data) throws -> JSONDictionary? {
        var response: JSONDictionary?
        
        do {
            response = try JSONSerialization.jsonObject(with: data, options: []) as? JSONDictionary
        }
        catch {
            throw IMDBNetworkError.JSONSerializationError
        }
        
        guard let responseStatus = response!["Response"] as? String else {
            throw IMDBNetworkError.InvalidResultDictionary
        }
        
        if responseStatus == "True" {
            return response
        }
        else {
            throw IMDBNetworkError.ResponseError(response?["Error"] as? String)
        }
    }
    
    private func searchDataProcessing(_ data: Data) throws -> SearchFilmResponce {
        var response: JSONDictionary?
        var searchFilmInfos = [SearchFilmInfo]()
        
        do {
            response = try dataProcessing(data)
        }
        catch {
            throw error
        }
        
        guard let filmsArray = response!["Search"] as? [Any], let totalResults = response!["totalResults"] as? String else {
            throw IMDBNetworkError.InvalidResultDictionary
        }
        
        for filmDictionary in filmsArray {
            if let filmDictionary = filmDictionary as? JSONDictionary, let id = filmDictionary["imdbID"] as? String, let title = filmDictionary["Title"] as? String {
                let searchFilmInfo = SearchFilmInfo(id: id, title: title)
                searchFilmInfos.append(searchFilmInfo)
            }
            else {
                throw IMDBNetworkError.DictionaryParsingError
            }
        }
        
        return SearchFilmResponce(films: searchFilmInfos, totalResults: Int(totalResults))
    }
    
    private func filmDataProcessing(_ data: Data) throws -> Film {
        var response: JSONDictionary?
        
        do {
            response = try dataProcessing(data)
        }
        catch {
            throw error
        }
        
        if let id = response!["imdbID"] as? String, let title = response!["Title"] as? String, let year = response!["Year"] as? String {
            let imdbRating = response!["imdbRating"] as? String
            let country = response!["Country"] as? String
            let director = response!["Director"] as? String
            let actors = response!["Actors"] as? String
            let plot = response!["Plot"] as? String
            let posterPath = response!["Poster"] as? String
            
            return Film(id: id, title: title, year: year, imdbRating: imdbRating, country: country, director: director, actors: actors, plot: plot, posterPath: posterPath)
        }
        else {
            throw IMDBNetworkError.DictionaryParsingError
        }
    }
    
}
