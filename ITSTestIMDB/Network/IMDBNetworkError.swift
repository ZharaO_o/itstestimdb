//
//  IMDBNetworkError.swift
//  ITSTestIMDB
//
//  Created by Ivan Babkin on 09.08.2018.
//  Copyright © 2018 Ivan Babkin. All rights reserved.
//

import Foundation

extension Error {
    var description: String {
        if let err = self as? IMDBNetworkError {
            switch err {
            case .ResponseError(let description):
                return "Response Error - \(description ?? "null")"
            case .HTTPResponseError(let code):
                return "HTTP Response Error code \(code)"
            default:
                return self.localizedDescription
            }
        }
        
        return self.localizedDescription
    }
}

enum IMDBNetworkError: Error {
    case InvalidResultDictionary
    case JSONSerializationError
    case DictionaryParsingError
    case ResponceUnknownError
    case HTTPResponseError(Int)
    case ResponseError(String?)
    case LoadingImageError
}
